package com.cagriyalcinsoy.t24.data.models.pojos;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsImages{
    private String list;
    private String box;
    private String page;
    private String grid;

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }
}
