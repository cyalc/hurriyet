package com.cagriyalcinsoy.t24.interactors;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public interface NewsDetailInteractor {
    void getNewsDetailById(String id);
}
