package com.cagriyalcinsoy.t24.utils;

import android.net.Uri;

/**
 * Created by cagriyalcinsoy on 04/04/16.
 */
public class UrlUtils {
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    /**
     * Strip the url string for unwanted characters
     * @param url -> url with illegal characters
     * @return url -> url without illegal characters
     */
    public static String fixImageUrl(String url) {
        return "http://" + Uri.encode(url, ALLOWED_URI_CHARS).substring(2);
    }
}
