package com.cagriyalcinsoy.t24.ui.news_main;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public interface NewsMainPresenter {
    void getNewsByPageNumber(int pageNumber);
    void onResume();
    void onPause();
}
