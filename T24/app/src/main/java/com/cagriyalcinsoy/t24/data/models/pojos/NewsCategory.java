package com.cagriyalcinsoy.t24.data.models.pojos;


/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsCategory {
    private String id;
    private String name;
    private String alias;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
