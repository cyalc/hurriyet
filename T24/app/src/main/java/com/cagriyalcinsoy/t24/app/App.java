package com.cagriyalcinsoy.t24.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGraph();
    }

    private void setupGraph() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent appComponent(){
        return appComponent;
    }
}
