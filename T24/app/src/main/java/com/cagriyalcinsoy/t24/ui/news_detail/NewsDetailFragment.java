package com.cagriyalcinsoy.t24.ui.news_detail;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.transition.Fade;
import android.transition.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cagriyalcinsoy.t24.R;
import com.cagriyalcinsoy.t24.app.AppComponent;
import com.cagriyalcinsoy.t24.ui.base.base_fragment.BaseFragment;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public class NewsDetailFragment extends BaseFragment implements NewsDetailView{

    @Bind(R.id.fragmentNewsDetail_tvTitle)
    TextView tvTitle;
    @Bind(R.id.fragmentNewsDetail_ivContent)
    ImageView ivContent;
    @Bind(R.id.fragmentNewsDetail_tvContent)
    TextView tvContent;

    @Inject NewsDetailPresenter newsDetailPresenter;
    @Inject
    Picasso picasso;

    public static NewsDetailFragment newInstance(String id){
        Bundle args = new Bundle();
        args.putString("newsId", id);
        NewsDetailFragment newsDetailFragment = new NewsDetailFragment();
        newsDetailFragment.setArguments(args);
        return newsDetailFragment;
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerNewsDetailComponent.builder().appComponent(appComponent).newsDetailModule(new NewsDetailModule(this)).build().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_detail, container, false);
        ButterKnife.bind(this, view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setEnterTransition(new Fade(Visibility.MODE_IN));
            setExitTransition(new Fade(Fade.OUT));
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newsDetailPresenter.getNewsDetailById(getArguments().getString("newsId"));
    }

    @Override
    public void onResume() {
        super.onResume();
        newsDetailPresenter.onResume();
    }

    @Override
    public void onPause() {
        newsDetailPresenter.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void setTitle(String title) {
        tvTitle.setText(Html.fromHtml(title));
    }

    @Override
    public void setImageView(String imageUrl) {
        picasso.load(imageUrl).into(ivContent);
    }

    @Override
    public void setContent(String content) {
        tvContent.setText(Html.fromHtml(content));
    }
}
