package com.cagriyalcinsoy.t24.interactors;

import android.util.Log;

import com.cagriyalcinsoy.t24.data.NewsService;
import com.cagriyalcinsoy.t24.data.models.pojos.News;
import com.cagriyalcinsoy.t24.data.models.pojos.NewsData;
import com.cagriyalcinsoy.t24.utils.RxBus;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsInteractorImpl implements NewsInteractor {
    private final String TAG = getClass().getSimpleName();
    private NewsService newsService;

    private RxBus rxBus;

    public NewsInteractorImpl(NewsService newsService, RxBus rxBus) {
        this.newsService = newsService;
        this.rxBus = rxBus;
    }

    @Override
    public void getNewsByPageNumber(final int pageNumber) {
        newsService.getNewsListByPage(pageNumber)
                .map(new Func1<News, ArrayList<NewsData>>() {
                    @Override
                    public ArrayList<NewsData> call(News news) {
                        return news.getData();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<NewsData>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.toString());
                    }

                    @Override
                    public void onNext(ArrayList<NewsData> newsDatas) {
                        rxBus.send(newsDatas);
                    }
                });
    }

}
