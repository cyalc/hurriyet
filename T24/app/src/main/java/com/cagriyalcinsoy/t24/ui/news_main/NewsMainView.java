package com.cagriyalcinsoy.t24.ui.news_main;

import com.cagriyalcinsoy.t24.data.models.pojos.NewsData;
import com.cagriyalcinsoy.t24.ui.base.base_activity.BaseView;

import java.util.List;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public interface NewsMainView extends BaseView{
    void setAdapter(List<NewsData> newsDatas);
    void addData(List<NewsData> newsDatas);
}
