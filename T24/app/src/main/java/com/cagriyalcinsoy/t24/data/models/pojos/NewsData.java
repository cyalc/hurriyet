package com.cagriyalcinsoy.t24.data.models.pojos;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsData {
    private String id;
    private String title;
    private String excerpt;
    private String alias;
    private NewsUrls urls;
    private NewsCategory category;
    private NewsStats stats;
    private NewsImages images;
    private String publishingDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public NewsUrls getUrls() {
        return urls;
    }

    public void setUrls(NewsUrls urls) {
        this.urls = urls;
    }

    public NewsCategory getCategory() {
        return category;
    }

    public void setCategory(NewsCategory category) {
        this.category = category;
    }

    public NewsStats getStats() {
        return stats;
    }

    public void setStats(NewsStats stats) {
        this.stats = stats;
    }

    public NewsImages getImages() {
        return images;
    }

    public void setImages(NewsImages images) {
        this.images = images;
    }


    public String getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(String publishingDate) {
        this.publishingDate = publishingDate;
    }
}
