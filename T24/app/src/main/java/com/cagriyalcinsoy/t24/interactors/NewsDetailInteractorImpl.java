package com.cagriyalcinsoy.t24.interactors;

import android.util.Log;

import com.cagriyalcinsoy.t24.data.NewsService;
import com.cagriyalcinsoy.t24.data.models.pojos.NewsDetail;
import com.cagriyalcinsoy.t24.data.models.pojos.NewsDetailData;
import com.cagriyalcinsoy.t24.utils.RxBus;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public class NewsDetailInteractorImpl implements NewsDetailInteractor {
    
    private NewsService newsService;
    private RxBus rxBus;

    public NewsDetailInteractorImpl(NewsService newsService, RxBus rxBus) {
        this.newsService = newsService;
        this.rxBus = rxBus;
    }


    @Override
    public void getNewsDetailById(String id) {
        newsService.getNewsDetailById(id)
                .map(new Func1<NewsDetail, NewsDetailData>() {

                    @Override
                    public NewsDetailData call(NewsDetail newsDetail) {
                        return newsDetail.getData();
                    }
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NewsDetailData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("", "onError: ", e);
                    }

                    @Override
                    public void onNext(NewsDetailData newsDetailDatas) {
                        rxBus.send(newsDetailDatas);
                    }
                });
    }
}
