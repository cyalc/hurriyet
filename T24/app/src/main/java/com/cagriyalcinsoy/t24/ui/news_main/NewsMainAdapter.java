package com.cagriyalcinsoy.t24.ui.news_main;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cagriyalcinsoy.t24.R;
import com.cagriyalcinsoy.t24.data.models.pojos.NewsData;
import com.cagriyalcinsoy.t24.utils.UrlUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsMainAdapter extends RecyclerView.Adapter<NewsMainAdapter.ViewHolder> {

    private Picasso picasso;
    private List<NewsData> mData;
    private OnClickListener onClickListener;

    public NewsMainAdapter(Picasso picasso, List<NewsData> mData) {
        this.picasso = picasso;
        this.mData = mData;
    }


    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_main, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NewsData newsData = mData.get(position);
        holder.tvTitle.setText(Html.fromHtml(newsData.getTitle()));
        holder.tvCategory.setText(newsData.getCategory().getName());
        picasso.load(UrlUtils.fixImageUrl(newsData.getImages().getPage())).fit().centerCrop().into(holder.ivContent);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClicked(newsData.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.itemNewsMain_tvTitle)
        TextView tvTitle;
        @Bind(R.id.itemNewsMain_tvCategory)
        TextView tvCategory;
        @Bind(R.id.itemNewsMain_ivContent)
        ImageView ivContent;
        @Bind(R.id.itemNewsMain_cardViewRoot)
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void addData(List<NewsData> newsDatas) {
        List<NewsData> newData = new ArrayList<>(newsDatas.size());
        for (int i = 0; i < newsDatas.size(); i++) {
            newData.add(newsDatas.get(i));
        }

        for (int i = 0; i < newData.size(); i++) {
            mData.add(newData.get(i));
        }
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onItemClicked(String id);
    }
}
