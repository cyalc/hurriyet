package com.cagriyalcinsoy.t24.ui.news_main;

import com.cagriyalcinsoy.t24.data.models.pojos.NewsData;
import com.cagriyalcinsoy.t24.interactors.NewsInteractor;
import com.cagriyalcinsoy.t24.utils.RxBus;

import java.util.List;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsMainPresenterImpl implements NewsMainPresenter {

    private NewsMainView newsMainView;
    private NewsInteractor newsInteractor;
    private RxBus rxBus;
    private CompositeSubscription compositeSubscription;
    private int pageNumber = 0;


    public NewsMainPresenterImpl(NewsMainView newsMainView, NewsInteractor newsInteractor, RxBus rxBus) {
        this.newsMainView = newsMainView;
        this.newsInteractor = newsInteractor;
        this.rxBus = rxBus;
    }


    @Override
    public void getNewsByPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;

        if(pageNumber == 0)
            newsMainView.showProgressLoading();

        newsInteractor.getNewsByPageNumber(pageNumber);
    }

    @Override
    public void onResume() {
        compositeSubscription = new CompositeSubscription();

        compositeSubscription.add(rxBus.toObserverable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                newsMainView.hideProgressLoading();
                if (o instanceof List<?>) {
                    if (pageNumber == 0) {
                        List<NewsData> newsDatas = (List<NewsData>) o;
                        newsMainView.setAdapter(newsDatas);

                    } else {
                        List<NewsData> newsDatas = (List<NewsData>) o;
                        newsMainView.addData(newsDatas);
                    }
                }
            }
        }));
    }

    @Override
    public void onPause() {
        compositeSubscription.unsubscribe();
    }


}
