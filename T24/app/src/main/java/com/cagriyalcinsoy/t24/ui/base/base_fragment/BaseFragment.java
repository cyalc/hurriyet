package com.cagriyalcinsoy.t24.ui.base.base_fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.cagriyalcinsoy.t24.app.App;
import com.cagriyalcinsoy.t24.app.AppComponent;
import com.cagriyalcinsoy.t24.ui.MainActivity;
import com.cagriyalcinsoy.t24.ui.base.base_activity.BaseActivity;
import com.cagriyalcinsoy.t24.ui.base.base_activity.BaseView;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public abstract class BaseFragment extends Fragment implements BaseView{

    /**
     * start injection for fragments
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(App.get(getActivity()).appComponent());
    }

    protected abstract void setupComponent(AppComponent appComponent);

    @Override
    public void showProgressLoading() {
        ((MainActivity)getActivity()).showProgressLoading();
    }

    @Override
    public void hideProgressLoading() {
        ((MainActivity)getActivity()).hideProgressLoading();
    }
}


