package com.cagriyalcinsoy.t24.ui.news_detail;

import com.cagriyalcinsoy.t24.data.models.pojos.NewsDetailData;
import com.cagriyalcinsoy.t24.interactors.NewsDetailInteractor;
import com.cagriyalcinsoy.t24.utils.RxBus;
import com.cagriyalcinsoy.t24.utils.UrlUtils;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public class NewsDetailPresenterImpl implements NewsDetailPresenter {

    private NewsDetailView newsDetailView;
    private RxBus rxBus;
    private NewsDetailInteractor newsDetailInteractor;
    private CompositeSubscription compositeSubscription;


    public NewsDetailPresenterImpl(NewsDetailView newsDetailView, RxBus rxBus, NewsDetailInteractor newsDetailInteractor) {
        this.newsDetailView = newsDetailView;
        this.rxBus = rxBus;
        this.newsDetailInteractor = newsDetailInteractor;
    }

    @Override
    public void getNewsDetailById(String id) {
        newsDetailView.showProgressLoading();
        newsDetailInteractor.getNewsDetailById(id);
    }

    @Override
    public void onResume() {
        compositeSubscription = new CompositeSubscription();
        compositeSubscription.add(rxBus.toObserverable().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if(o instanceof NewsDetailData){
                    NewsDetailData data = (NewsDetailData) o;
                    newsDetailView.setTitle(data.getTitle());
                    newsDetailView.setImageView(UrlUtils.fixImageUrl(data.getImages().getPage()));
                    newsDetailView.setContent(data.getText());
                    newsDetailView.hideProgressLoading();
                }
            }
        }));
    }

    @Override
    public void onPause() {
        compositeSubscription.unsubscribe();
    }


}
