package com.cagriyalcinsoy.t24.data.models.pojos;

import java.util.ArrayList;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class News {
    private ArrayList<NewsData> data = new ArrayList<>();
    private Paging paging;
    private boolean result;



    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ArrayList<NewsData> getData() {
        return data;
    }

    public void setData(ArrayList<NewsData> data) {
        this.data = data;
    }
}
