package com.cagriyalcinsoy.t24.data;

import com.cagriyalcinsoy.t24.data.models.pojos.News;
import com.cagriyalcinsoy.t24.data.models.pojos.NewsDetail;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public interface NewsService {
    @GET("/api/v3/stories.json")
    Observable<News> getNewsListByPage(@Query("paging") int pageNumber);

    @GET("api/v3/stories.json")
    Observable<NewsDetail> getNewsDetailById(@Query("story") String id);
}
