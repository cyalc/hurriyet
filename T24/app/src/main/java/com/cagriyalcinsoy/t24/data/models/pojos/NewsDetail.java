package com.cagriyalcinsoy.t24.data.models.pojos;


/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public class NewsDetail {
    private NewsDetailData data;
    private boolean result;

    public NewsDetail(NewsDetailData data) {
        this.data = data;
    }


    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public NewsDetailData getData() {
        return data;
    }

    public void setData(NewsDetailData data) {
        this.data = data;
    }
}
