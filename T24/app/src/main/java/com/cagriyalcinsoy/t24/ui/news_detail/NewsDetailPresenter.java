package com.cagriyalcinsoy.t24.ui.news_detail;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public interface NewsDetailPresenter{
    void getNewsDetailById(String id);
    void onResume();
    void onPause();
}
