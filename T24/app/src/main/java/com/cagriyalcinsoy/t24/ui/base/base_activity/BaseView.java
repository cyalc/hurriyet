package com.cagriyalcinsoy.t24.ui.base.base_activity;

/**
 * Created by cagriyalcinsoy on 18/04/16.
 */
public interface BaseView {
    void showProgressLoading();
    void hideProgressLoading();
}
