package com.cagriyalcinsoy.t24.ui.news_main;

import com.cagriyalcinsoy.t24.app.AppComponent;
import com.cagriyalcinsoy.t24.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = NewsMainModule.class
)
public interface NewsMainComponenet {
    void inject(NewsMainFragment newsMainFragment);

    NewsMainPresenter getNewsMainPresenter();
}
