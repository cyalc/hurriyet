package com.cagriyalcinsoy.t24.interactors;

import com.cagriyalcinsoy.t24.data.NewsService;
import com.cagriyalcinsoy.t24.utils.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
@Module
public class InteractorsModule {

    @Provides
    @Singleton
    public NewsInteractor provideNewsInteractor(NewsService newsService, RxBus bus) {
        return new NewsInteractorImpl(newsService, bus);
    }

    @Provides
    @Singleton
    public NewsDetailInteractor provideNewsDetailInteractor(NewsService newsService, RxBus rxBus){
        return new NewsDetailInteractorImpl(newsService, rxBus);
    }


}
