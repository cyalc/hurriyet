package com.cagriyalcinsoy.t24.ui.news_main;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Visibility;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cagriyalcinsoy.t24.R;
import com.cagriyalcinsoy.t24.app.AppComponent;
import com.cagriyalcinsoy.t24.data.models.pojos.NewsData;
import com.cagriyalcinsoy.t24.ui.base.base_fragment.BaseFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class NewsMainFragment extends BaseFragment implements NewsMainView, NewsMainAdapter.OnClickListener {

    @Bind(R.id.fragmentNewsMain_rVNews)
    RecyclerView lvNews;

    NewsMainAdapter newsMainAdapter;

    @Inject
    Picasso picasso;
    @Inject
    NewsMainPresenter newsMainPresenter;

    private RecyclerView.LayoutManager layoutManager;
    private boolean mIsLoading = false;
    private boolean mIsLastPage = false;
    public static final int PAGE_SIZE = 3;

    private NewsMainFragmentCallback newsMainFragmentCallback;
    private int pageNumber = 0;

    private RecyclerView.OnScrollListener
            mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView,
                                         int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

            if (!mIsLoading && !mIsLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
        }
    };

    private void loadMoreItems() {
        newsMainPresenter.getNewsByPageNumber(++pageNumber);
    }

    public interface NewsMainFragmentCallback {
        void toDetailFragment(String id);
        void onNewsMainViewCreated();
    }

    public static NewsMainFragment newInstance() {
        return new NewsMainFragment();
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerNewsMainComponenet.builder().appComponent(appComponent).newsMainModule(new NewsMainModule(this)).build().inject(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            newsMainFragmentCallback = (NewsMainFragmentCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement NewsMainFragmentCallback");
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_main, container, false);
        ButterKnife.bind(this, view);
        lvNews.setHasFixedSize(true);

        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        } else {
            layoutManager = new GridLayoutManager(getContext(), 2);
        }

        lvNews.setLayoutManager(layoutManager);
        lvNews.addOnScrollListener(mRecyclerViewOnScrollListener);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newsMainFragmentCallback.onNewsMainViewCreated();
        pageNumber = 0;
        newsMainPresenter.getNewsByPageNumber(pageNumber);

    }

    @Override
    public void onResume() {
        super.onResume();
        newsMainPresenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        newsMainPresenter.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void setAdapter(List<NewsData> newsDatas) {
        newsMainAdapter = new NewsMainAdapter(picasso, newsDatas);
        lvNews.setAdapter(newsMainAdapter);
        newsMainAdapter.setOnClickListener(this);
    }

    @Override
    public void addData(List<NewsData> newsDatas) {
        newsMainAdapter.addData(newsDatas);
    }

    @Override
    public void onItemClicked(String id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setExitTransition(new Fade(Visibility.MODE_OUT));
        }
        newsMainFragmentCallback.toDetailFragment(id);
    }
}
