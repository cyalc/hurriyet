package com.cagriyalcinsoy.t24.ui.news_main;

import com.cagriyalcinsoy.t24.interactors.NewsInteractor;
import com.cagriyalcinsoy.t24.utils.RxBus;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */

@Module
public class NewsMainModule {
    private NewsMainView view;

    public NewsMainModule(NewsMainView view) {
        this.view = view;
    }

    @Provides
    public NewsMainView provideView(){
        return view;
    }

    @Provides NewsMainPresenter providePresenter(NewsMainView newsMainView, NewsInteractor newsInteractor, RxBus bus){
        return new NewsMainPresenterImpl(newsMainView, newsInteractor, bus);
    }
}
