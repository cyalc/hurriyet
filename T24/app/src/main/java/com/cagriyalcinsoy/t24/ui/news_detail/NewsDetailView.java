package com.cagriyalcinsoy.t24.ui.news_detail;

import com.cagriyalcinsoy.t24.ui.base.base_activity.BaseView;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public interface NewsDetailView extends BaseView{
    void setTitle(String title);
    void setImageView(String imageUrl);
    void setContent(String content);
}
