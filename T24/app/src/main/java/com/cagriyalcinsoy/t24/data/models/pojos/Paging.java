package com.cagriyalcinsoy.t24.data.models.pojos;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public class Paging {
    private int current;
    private int limit;
    private int pages;
    private int items;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }
}
