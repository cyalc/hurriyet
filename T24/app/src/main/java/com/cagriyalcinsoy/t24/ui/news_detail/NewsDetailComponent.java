package com.cagriyalcinsoy.t24.ui.news_detail;

import com.cagriyalcinsoy.t24.app.AppComponent;
import com.cagriyalcinsoy.t24.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 * ioc container for news detail pages (lives along the fragment)(fragment scope)
 */
@FragmentScope
@Component(
        dependencies = AppComponent.class,
        modules = NewsDetailModule.class
)
public interface NewsDetailComponent {
    void inject(NewsDetailFragment newsDetailFragment);
    NewsDetailPresenter getNewsDetailPresenter();
}
