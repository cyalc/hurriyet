package com.cagriyalcinsoy.t24.data.models.pojos;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
public class NewsDetailData extends NewsData {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
