package com.cagriyalcinsoy.t24.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.cagriyalcinsoy.t24.R;
import com.cagriyalcinsoy.t24.app.AppComponent;
import com.cagriyalcinsoy.t24.ui.base.base_activity.BaseActivity;
import com.cagriyalcinsoy.t24.ui.news_detail.NewsDetailFragment;
import com.cagriyalcinsoy.t24.ui.news_main.NewsMainFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements NewsMainFragment.NewsMainFragmentCallback {

    @Bind(R.id.activity_main_toolbar)
    Toolbar toolbar;

    @Bind(R.id.activity_main_progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsDetailFragment newsDetailFragment = (NewsDetailFragment) getSupportFragmentManager().findFragmentByTag(NewsDetailFragment.class.getSimpleName());
                if (newsDetailFragment != null) {
                    onBackPressed();
                }
            }
        });

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.activity_main_container, NewsMainFragment.newInstance(), NewsMainFragment.class.getSimpleName())
                    .commit();
        }

    }

    @Override
    protected void setupComponent(AppComponent appComponent) {

    }

    @Override
    public void toDetailFragment(String id) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_container, NewsDetailFragment.newInstance(id), NewsDetailFragment.class.getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onNewsMainViewCreated() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }


    @Override
    public void showProgressLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }
}
