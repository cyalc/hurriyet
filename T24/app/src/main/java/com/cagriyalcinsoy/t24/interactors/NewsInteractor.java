package com.cagriyalcinsoy.t24.interactors;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public interface NewsInteractor {
    void getNewsByPageNumber(int pageNumber);
}
