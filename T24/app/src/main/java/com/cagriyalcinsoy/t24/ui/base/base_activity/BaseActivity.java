package com.cagriyalcinsoy.t24.ui.base.base_activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cagriyalcinsoy.t24.app.App;
import com.cagriyalcinsoy.t24.app.AppComponent;

import javax.inject.Inject;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView{

    /**
     * start injection for activities
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(App.get(this).appComponent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected abstract void setupComponent(AppComponent appComponent);

}
