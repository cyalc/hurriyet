package com.cagriyalcinsoy.t24.ui.news_detail;

import com.cagriyalcinsoy.t24.data.models.pojos.NewsDetail;
import com.cagriyalcinsoy.t24.interactors.NewsDetailInteractor;
import com.cagriyalcinsoy.t24.utils.RxBus;

import dagger.Module;
import dagger.Provides;

/**
 * Created by cagriyalcinsoy on 10/04/16.
 */
@Module
public class NewsDetailModule {

    private NewsDetailView view;

    public NewsDetailModule(NewsDetailView view) {
        this.view = view;
    }

    @Provides
    public NewsDetailView provideNewsDetailView() {
        return view;
    }

    @Provides
    NewsDetailPresenter provideNewsDetailPresenter(NewsDetailInteractor newsDetailInteractor, RxBus rxBus, NewsDetailView newsDetailView) {
        return new NewsDetailPresenterImpl(newsDetailView, rxBus, newsDetailInteractor);
    }
}
