package com.cagriyalcinsoy.t24.app;

import com.cagriyalcinsoy.t24.data.DataModule;
import com.cagriyalcinsoy.t24.interactors.InteractorsModule;
import com.cagriyalcinsoy.t24.interactors.NewsDetailInteractor;
import com.cagriyalcinsoy.t24.interactors.NewsInteractor;
import com.cagriyalcinsoy.t24.utils.RxBus;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by cagriyalcinsoy on 03/04/16.
 *
 * IoC container on application level. These dependencies live along the application.(Singleton)
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                DataModule.class,
                InteractorsModule.class
        }
)
public interface AppComponent {
    void inject(App app);

    Picasso getPicasso();
    Bus getBus();
    RxBus getRxBus();

    //Interactors
    NewsInteractor getNewsInteractor();
    NewsDetailInteractor getNewsDetailInteractor();
}
